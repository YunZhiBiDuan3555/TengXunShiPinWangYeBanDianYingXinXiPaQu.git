# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
import pymysql
from scrapy.conf import settings

class MySQLConnect(scrapy.Item):
    @staticmethod
    def mysqlConnect(sql):
        host = settings['MYSQL_HOSTS']
        user = settings['MYSQL_USER']
        psd = settings['MYSQL_PASSWORD']
        db = settings['MYSQL_DB']
        charset = settings['CHARSET']
        port = settings['MYSQL_PORT']
        # 数据库连接
        con = pymysql.connect(host=host, user=user, passwd=psd, db=db, charset=charset, port=port)
    # 数据库游标
        cur = con.cursor()
        try:
            cur.execute(sql)
            print("insert success")  # 测试语句
        except Exception as e:
            print('Insert error:', e)
            con.rollback()
        else:
            con.commit()
        con.close()

class TencentItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    #简介区
    abstract=scrapy.Field()
    director=scrapy.Field()
    director_link=scrapy.Field()
    act=scrapy.Field()
    act_link=scrapy.Field()

    #概览区
    movie_title=scrapy.Field()
    score=scrapy.Field()
    movie_playCount=scrapy.Field()
    movie_account=scrapy.Field()
    movie_link=scrapy.Field()

    def insert_data(self,item):
        sql1 = "insert into abstract(movieTitle,director,directorLink,act,actLink,abstract)values('%s','%s','%s','%s','%s','%s');" % (item['movie_title'], item['director'], item['director_link'], item['act'], item['act_link'],item['abstract'])
        print('TencentItem insert.....................')
        MySQLConnect.mysqlConnect(sql1)
        sql3 = "insert into overview(movieTitle,score,playCount,link)values('%s','%s','%s','%s');" % (item['movie_title'], item['score'], item['movie_playCount'], item['movie_link'])
        MySQLConnect.mysqlConnect(sql3)
class CommentItem(scrapy.Item):
    # 评论区
    movie_title = scrapy.Field()
    timeDifference = scrapy.Field()
    content = scrapy.Field()
    up = scrapy.Field()
    rep = scrapy.Field()
    userLink = scrapy.Field()
    userid = scrapy.Field()
    def insert_data(self,item):
        sql2 = "insert into comment(userID,userLink,timeDiffrence,content,praise,ref,movieTitle)values('%s','%s','%s','%s','%s','%s','%s');" % (item['userid'], item['userLink'], item['timeDifference'], item['content'], item['up'],item['rep'], item['movie_title'])
        print('CommentItem insert................')
        MySQLConnect.mysqlConnect(sql2)