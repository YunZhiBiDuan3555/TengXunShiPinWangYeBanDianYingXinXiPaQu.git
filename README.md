# 腾讯视频网页版电影信息爬取
主要是采用scrapy框架和MySQL数据库存储进行信息爬取
 **安装准备** 
python35环境（本人所用环境）
mysql数据库
安装python包
scrapy
lxml(使用xpath()对网页源代码中需要内容快速爬取)
pymysql
re
json
requests

 **提前建立数据库** 
建表详见tensql.sql
具体连接数据库以及配置情况可见项目内文件items.py和setttings.py的配置情况比对后做出相应修改
当然可以去博客：http://blog.csdn.net/sunchao3555/article/details/79206990
 **说明** 

本项目中评论区代码采用json解析,和正则匹配等方式，有些繁琐，希望有意者可以帮助优化，感激不尽


