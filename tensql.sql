CREATE TABLE overview(
  movieTitle VARCHAR(255)NOT NULL PRIMARY KEY,
  score VARCHAR(10),
  playCount VARCHAR(10),
  link VARCHAR(255)
)DEFAULT CHARSET=utf8;
CREATE TABLE abstract(
  movieTitle VARCHAR(255)NOT NULL PRIMARY KEY,
  director VARCHAR(20),
  directorLink VARCHAR(200),
  act VARCHAR(200),
  actLink VARCHAR(255),
  abstract VARCHAR(10000)
)DEFAULT CHARSET=utf8;
CREATE TABLE comment(
  userID VARCHAR (20) NOT NULL  PRIMARY KEY ,
  userLink VARCHAR (255),
  timeDiffrence VARCHAR (20)PRIMARY KEY ,
  content VARCHAR (10000),
  praise VARCHAR (10),
  ref VARCHAR (10),
  movieTitle VARCHAR(255) PRIMARY KEY
)DEFAULT CHARSET=utf8;